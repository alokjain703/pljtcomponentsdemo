import { MatCardModule } from '@angular/material/card';

import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule, MatFormFieldModule, MatInputModule,
  MatCheckboxModule, MatIconModule, MatTooltipModule, MatDialogModule
} from '@angular/material';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColorPickerModule } from 'ngx-color-picker';
import { StickyNoteViewComponent } from './pljt-stickynote-module/components/sticky-note-view/sticky-note-view.component';
import { StickyNoteFilterPipe } from './pljt-stickynote-module/pipes/stickynote.filter.pipe';
import { PljtConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    StickyNoteViewComponent, StickyNoteFilterPipe,
    PljtConfirmDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: StickyNoteViewComponent },
    ]),
    ColorPickerModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    MatCheckboxModule,
    MatIconModule,
    MatTooltipModule,
    MatCardModule,
    MatDialogModule
  ],
  exports: [
    StickyNoteViewComponent, PljtConfirmDialogComponent
  ],
  entryComponents: [PljtConfirmDialogComponent]
})
export class PljtStickyNoteModule { }
