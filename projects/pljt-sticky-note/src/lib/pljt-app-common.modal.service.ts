import { Observable } from 'rxjs';
import { PljtConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class PljtAppCommonModalService {

  constructor(
    private dialog: MatDialog,
  ) { }

  public showConfirmDialog(title: string, message: string,
    okButtonString: string, cancelButtonString: string, warningMessage = ''): Observable<boolean> {

    let dialogRef: MatDialogRef<PljtConfirmDialogComponent>;

    dialogRef = this.dialog.open(PljtConfirmDialogComponent, {
      disableClose: true
    });
    dialogRef.componentInstance.title = title;
    dialogRef.componentInstance.message = message;
    dialogRef.componentInstance.warningMessage = warningMessage;
    dialogRef.componentInstance.cancelButtonString = cancelButtonString;
    dialogRef.componentInstance.okButtonString = okButtonString;

    return dialogRef.afterClosed();
  }

}
