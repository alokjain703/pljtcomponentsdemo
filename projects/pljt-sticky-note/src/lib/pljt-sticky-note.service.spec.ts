import { MatIconModule } from '@angular/material/icon';
import { TestBed } from '@angular/core/testing';

import { PljtStickyNoteService } from './pljt-sticky-note.service';
import { MatCardModule, MatCheckboxModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatTooltipModule, 
  MatDialogModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { StickyNoteViewComponent, StickyNoteFilterPipe } from '../public_api';
import { PljtConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

describe('PljtStickyNoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [
      StickyNoteViewComponent, StickyNoteFilterPipe,
      PljtConfirmDialogComponent
    ],
    imports: [
      CommonModule,
      FormsModule,
      ColorPickerModule,
      MatButtonModule,
      MatFormFieldModule,
      MatInputModule,
      FlexLayoutModule,
      MatCheckboxModule,
      MatIconModule,
      MatTooltipModule,
      MatCardModule,
      MatDialogModule
    ]

  }));

  it('should be created', () => {
    const service: PljtStickyNoteService = TestBed.get(PljtStickyNoteService);
    expect(service).toBeTruthy();
  });
});
