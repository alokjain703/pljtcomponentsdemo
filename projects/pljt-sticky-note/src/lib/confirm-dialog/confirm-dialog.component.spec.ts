import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PljtConfirmDialogComponent } from './confirm-dialog.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatCheckboxModule, MatIconModule, 
  MatTooltipModule, MatCardModule, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

describe('PljtConfirmDialogComponent', () => {
  let component: PljtConfirmDialogComponent;
  let fixture: ComponentFixture<PljtConfirmDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PljtConfirmDialogComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ColorPickerModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        FlexLayoutModule,
        MatCheckboxModule,
        MatIconModule,
        MatTooltipModule,
        MatCardModule,
        MatDialogModule
      ],
      providers: [
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: [] },
        // ...
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PljtConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
