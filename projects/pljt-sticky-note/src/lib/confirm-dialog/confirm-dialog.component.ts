import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'pljt-sticky-note-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
// Use the service to call the Dialog.
export class PljtConfirmDialogComponent implements OnInit {

  public title = 'Please Confirm' ;
  public message: string;
  public warningMessage = '';
  public okButtonString = 'Yes';
  public cancelButtonString = 'No';

  constructor(public dialogRef: MatDialogRef<PljtConfirmDialogComponent>) {

  }

  ngOnInit() {
  }

}

