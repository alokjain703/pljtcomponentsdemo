import { IStickyNote, IStickyNotesList, StickyNote } from '../../models/pljt-stickynote-list.model.public';
import { Component, OnInit, Input } from '@angular/core';
import { PljtAppCommonModalService } from '../../../pljt-app-common.modal.service';

/* 
Library Component 
@author: Alok Jain
Stand Alone linbrary component.

*/

@Component({
  selector: 'pljt-sticky-note-view',
  templateUrl: './sticky-note-view.component.html',
  styleUrls: ['./sticky-note-view.component.scss']
})
export class StickyNoteViewComponent implements OnInit {

  /** Calling Application can send in the following:
   *  1) orgId and owner -- service will get the list from storage
   *  2) stickyNotesList
   */

  @Input() orgId = 'none';
  @Input() owner = 'defaultUser';
  // if no list is passed in input, then show default list
  @Input() stickyNotesList: IStickyNotesList = {
    orgId: this.orgId,
    owner: this.owner,
    list: [
      {
        id: '1', title: 'Title #1 from model', isPinned: false, color: '', position: 1,
        tasks: [{ text: 'Text content #1', completed: false }, { text: 'Text content #2', completed: true }]
      },
      {
        id: '2', title: 'Title #2 new', isPinned: false, color: '', position: 1,
        tasks: [{ text: 'Text content #2-1', completed: false }, { text: 'Text content #2-2', completed: false }]
      },
      {
        id: '3', title: 'Title #3 new', isPinned: false, color: '', position: 1,
        tasks: [{ text: 'Text content #3-1', completed: false }, { text: 'Text content #3-2', completed: false }]
      },
      {
        id: '4', title: 'Title #4 new', isPinned: true, color: '', position: 1,
        tasks: [{ text: 'Text content #4-1', completed: false }, { text: 'Text content #4-2', completed: false }]
      },
    ]
  };

  onStickyNoteHover = false;
  mouseOverIndex = -1;
  editMode = 'view';
  currentStickyNote: IStickyNote;
  currentStickyNoteindex = 1;
  newTask = '';
  newTaskErrorMessage = 'Task title and at least one task is required';
  errorMessage = '';
  tooltipOnButtonDelay = 750;
  stickyNoteFilterSearchStr = '';

  stickyNoteSizes = {
    small: { size: 'small', height: '10em', width: '10em' },
    medium: { size: 'medium', height: '13em', width: '13em' },
    large: { size: 'large', height: '18em', width: '18em' }
  };

  currentStickyNoteSizeObject = this.stickyNoteSizes.small;

  constructor(
    private pljtAppCommonModalService: PljtAppCommonModalService,
  ) { }

  ngOnInit() {
    console.log('StickyNoteViewComponent stickyNotesList', this.stickyNotesList);
    this.sortStickyNotesByPinned(this.stickyNotesList);
  }

  sortStickyNotesByPinned(stickyNotesList: IStickyNotesList) {
    if (stickyNotesList && stickyNotesList.list && stickyNotesList.list.length > 1) {
      stickyNotesList.list.sort(function (a, b) {
        if (a.isPinned && !b.isPinned) {
          return -1;
        }
        if (!a.isPinned && b.isPinned) {
          return 1;
        }
        return 0;
      });
    }
  }


  stickyNoteMouseOut() {
    this.mouseOverIndex = -1;
  }

  stickyNoteMouseOver(index) {
    this.mouseOverIndex = index;
  }

  stickyNoteClicked(stickyNote: IStickyNote, index: number) {
    console.log('stickyNoteClicked stickyNote', stickyNote);
    this.editMode = 'edit';
    this.currentStickyNote = stickyNote;
    this.currentStickyNoteindex = index;
  }

  doneEditingStickyNote() {
    this.errorMessage = '';
    console.log(' doneEditingStickyNote currentStickyNote', this.currentStickyNote);
    if (this.newTask) {
      this.currentStickyNote.tasks.push({ text: this.newTask, completed: false });
      this.newTask = '';
    }
    if (this.editMode === 'new' || this.editMode === 'edit') {
      if (this.currentStickyNote.title && this.currentStickyNote.tasks.length > 0) {
        this.errorMessage = '';
        if (this.editMode === 'new') {
          this.stickyNotesList.list.push(this.currentStickyNote);
        }
      } else {
        this.errorMessage = this.newTaskErrorMessage;
      }
    }
    if (this.errorMessage) {
      console.log(' doneEditingStickyNote this.errorMessage', this.errorMessage);
    } else {
      console.log(' doneEditingStickyNote if error message else', this.errorMessage);
      this.editMode = 'view';
    }
    this.sortStickyNotesByPinned(this.stickyNotesList);
    console.log('stickyNoteClicked stickyNote', this.currentStickyNote);
  }

  stickyNoteColorChanged(event) {
    console.log('stickyNoteColorChanged', event);
    this.currentStickyNote.color = event;
  }

  addNewTask() {
    console.log('addNewTask', this.newTask);
    this.errorMessage = '';
    if (this.newTask) {
      const newTaskItem = { text: this.newTask, completed: false };
      this.currentStickyNote.tasks.push(newTaskItem);
      this.newTask = '';
    }
  }

  deleteTask(index) {
    console.log('deleteTask', index);
    this.currentStickyNote.tasks.splice(index, 1);
  }

  addNewStickyNote() {
    console.log('addNewStickyNote');
    this.editMode = 'new';
    this.currentStickyNote = new StickyNote({ tasks: [] });
    console.log('this.currentStickyNote', this.currentStickyNote);
  }

  deleteStickyNote() {
    console.log('deleteStickyNote');
    this.openDeleteConfimDialog();
    // this.editMode = 'view';
  }

  cancelNewStickyNote() {
    this.editMode = 'view';
    this.currentStickyNote = new StickyNote({ tasks: [] });
  }

  changeStickyNoteSize(size: string) {
    console.log('size', size);
    this.currentStickyNoteSizeObject = this.stickyNoteSizes[size];
    console.log('currentStickyNoteSizeObject', this.currentStickyNoteSizeObject);
  }

  getBorder(size: string) {
    if (size === this.currentStickyNoteSizeObject.size) {
      return 'red solid 2px';
    } else {
      return 'none';
    }

  }

  openDeleteConfimDialog() {
    // use modal service to open the dialog
    this.pljtAppCommonModalService.showConfirmDialog('Confirmation Message', 'Are you sure you want to delete this Note ?',
      'Yes', 'No', 'This note will be deleted permanently !')
      .subscribe(res => {
        // this.result = res;
        if (res) {
          console.log(res);
          this.stickyNotesList.list.splice(this.currentStickyNoteindex, 1);
          this.editMode = 'view';
        } else {
          console.log(res);
        }
      });

  }


}
