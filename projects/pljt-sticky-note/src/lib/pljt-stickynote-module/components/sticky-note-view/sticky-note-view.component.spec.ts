import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StickyNoteViewComponent } from './sticky-note-view.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatCheckboxModule, 
  MatIconModule, MatTooltipModule, MatCardModule, MatDialogModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StickyNoteFilterPipe } from '../../pipes/stickynote.filter.pipe';
import { PljtConfirmDialogComponent } from '../../../confirm-dialog/confirm-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('StickyNoteViewComponent', () => {
  let component: StickyNoteViewComponent;
  let fixture: ComponentFixture<StickyNoteViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        StickyNoteViewComponent, StickyNoteFilterPipe,
        PljtConfirmDialogComponent
      ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        FormsModule,
        ColorPickerModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        FlexLayoutModule,
        MatCheckboxModule,
        MatIconModule,
        MatTooltipModule,
        MatCardModule,
        MatDialogModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StickyNoteViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
