import { IStickyNotesList } from '../models/pljt-stickynote-list.model.public';
export const DefaultStickyNotesList: IStickyNotesList = {
    orgId: 'none',
    owner: 'defaultUser',
    list: [
        {
            id: '1', title: 'Goals 2020', isPinned: false, color: '#ffc', position: 1,
            tasks: [
                { text: 'Get a good job', completed: false },
                { text: 'Buy a dream house', completed: false },
                { text: 'Travel to Europe', completed: true },
                { text: 'Buy a SUV', completed: false },
                { text: 'Trip to Alaska', completed: false },
                { text: 'Go on a cruise', completed: false }
            ]
        },
        {
            id: '2', title: 'Learn', isPinned: false, color: '#cfc', position: 1,
            tasks: [{ text: 'Kubernetes', completed: false }, { text: 'Helm', completed: false }]
        },
        {
            id: '3', title: 'Grocery', isPinned: false, color: '#ffc', position: 1,
            tasks: [{ text: 'Tomatoes', completed: false }, { text: 'Milk', completed: false }]
        },
        {
            id: '4', title: 'Urgent', isPinned: true, color: '#cfc', position: 1,
            tasks: [{ text: 'Finish Demo', completed: false }, { text: 'Complete Testing', completed: false }]
        },
        {
            id: '5', title: 'Kids', isPinned: false, color: '#ccf', position: 1,
            tasks: [{ text: 'Get Proj Material', completed: false }, { text: 'Dance competition', completed: false }]
        },
        {
            id: '4', title: 'Other', isPinned: false, color: '#cfc', position: 1,
            tasks: [{ text: 'pay taxes', completed: false }, { text: 'pay insurance', completed: false }]
        },
    ]
};
