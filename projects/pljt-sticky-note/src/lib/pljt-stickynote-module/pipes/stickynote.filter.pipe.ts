import { Pipe, PipeTransform } from '@angular/core';
import { IStickyNote, StickyNote } from '../models/pljt-stickynote-list.model.public';

@Pipe({
    name: 'stickyNoteFilter'
})
export class StickyNoteFilterPipe implements PipeTransform {
    transform(stickyNoteList: StickyNote[], filterString: string) {
        console.log(filterString);
        console.log('stickyNoteList', stickyNoteList);
        if (!stickyNoteList || !filterString) {
            return stickyNoteList;
        } else {
            filterString = filterString.toLocaleLowerCase();
            return stickyNoteList.filter(stickyNote => {
                if (stickyNote.title && stickyNote.title.toLowerCase().indexOf(filterString) !== -1) {
                    // Sticky Note Title match -- Return whole sticky note
                    return true;
                } else {
                    const tasks = stickyNote.tasks;
                    const filteredTasks = tasks.filter(task => task.text.indexOf(filterString) !== -1);
                    if (filteredTasks && filteredTasks.length > 0) {
                        // Tasks match -- Return sticky note with matched tasks only
                        stickyNote.tasks = filteredTasks;
                        return true;
                    } else {
                        return false;
                    }
                }
            });
        }
    }
}
