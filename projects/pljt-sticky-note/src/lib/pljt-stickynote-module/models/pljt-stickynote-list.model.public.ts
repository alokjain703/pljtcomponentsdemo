export interface IStickyNoteInput {
    orgId: string;
    owner: string;
}

export interface IStickyNotesList {
    orgId: string;
    owner: string;
    list: IStickyNote[];
}

export interface StickyNotesListOptions {
    orgId?: string;
    owner?: string;
    list?: IStickyNote[];
}

export class StickyNotesList implements IStickyNotesList {
    orgId: string;
    owner: string;
    list: IStickyNote[];

    constructor(options: StickyNotesListOptions = {}) {
        this.orgId = options.orgId || 'none';
        this.owner = options.owner || 'defaultUser';
        this.list = [...options.list] || [];
    }
}

// same as sticky NOTE
export interface IStickyNote {
    id: string;
    title: string;
    color: string;
    isPinned: boolean;
    position: number; // for sorting and display
    tasks: IStickyNoteItem[];
}

export interface StickyNoteOptions {
    id?: string;
    title?: string;
    color?: string;
    isPinned?: boolean;
    position?: number; // for sorting and display
    tasks?: IStickyNoteItem[];
}

export class StickyNote implements IStickyNote {
    id: string;
    title: string;
    color: string;
    isPinned: boolean;
    position: number; // for sorting and display
    tasks: IStickyNoteItem[];

    constructor(options: StickyNoteOptions = {}) {
        this.id = options.id || '';
        this.title = options.title || '';
        this.color = options.color || '#cfc';
        this.position = options.position || 1;
        this.isPinned = (typeof options.isPinned !== 'undefined') ? options.isPinned : false;
        this.tasks = [...options.tasks] || [];
    }

}

// ///// List item
export interface IStickyNoteItem {
    text: string;
    completed: boolean;
    // dueDate: Date;
}

export interface StickyNoteItemOptions {
    text?: string;
    completed?: boolean;
    // dueDate?: Date;
}

export class StickyNoteItem implements IStickyNoteItem {
    text: string;
    completed: boolean;
    // dueDate: Date;

    constructor(options: StickyNoteItemOptions = {}) {
        this.text = options.text || '';
        this.completed = (typeof options.completed !== 'undefined') ? options.completed : false;
        // this.dueDate = options.dueDate ;
    }
}


