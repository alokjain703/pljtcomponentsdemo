import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { IStickyNotesList, DefaultStickyNotesList } from 'pljt-sticky-note';
@Component({
  selector: 'app-dashboard',
  templateUrl: './app-dashboard.component.html',
  styleUrls: ['./app-dashboard.component.scss']
})
export class AppDashboardComponent {
  orgId = 'pljt';
  owner = 'defaultUser';
  stickyNotesList: IStickyNotesList = DefaultStickyNotesList;
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe([Breakpoints.Small, Breakpoints.Handset]).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { id: 'stickyNote', title: 'Sticky Notes', cols: 2, rows: 2 },
          { id: 'card2', title: 'Technologies/Frameworks', cols: 2, rows: 2 },
          { id: 'card3', title: 'Card 3', cols: 2, rows: 2 },
          { id: 'card4', title: 'Card 4', cols: 2, rows: 2 }
        ];
      }

      return [
        { id: 'stickyNote', title: 'Sticky notes', cols: 1, rows: 2 },
        { id: 'card2', title: 'Technologies/Frameworks', cols: 1, rows: 2 },
        { id: 'card3', title: 'Card 3', cols: 2, rows: 1 },
        { id: 'card4', title: 'Card 4', cols: 1, rows: 1 }
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) { }
}
