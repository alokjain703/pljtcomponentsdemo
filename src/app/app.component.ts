import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PLJT Components Demo';
  // constructor(private ViewportScroller: ViewportScroller){ this.viewportscroller; }
}
